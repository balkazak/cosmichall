$(function() {


	var swiper = new Swiper('.swiper-1', {
		loop: true,
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
	});
	var galleryThumbs = new Swiper('.gallery-thumbs', {
		spaceBetween: 10,
		slidesPerView: 4,
		// loop: true,
		freeMode: true,
		// loopedSlides: 5, //looped slides should be the same
		watchSlidesVisibility: true,
		watchSlidesProgress: true,
	});
	var galleryTop = new Swiper('.gallery-top', {
		spaceBetween: 10,
		// loop:true,
		// loopedSlides: 5, //looped slides should be the same
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		thumbs: {
			swiper: galleryThumbs,
		},
	});
	var galleryThumbs1 = new Swiper('.gallery-thumbs-1', {
		spaceBetween: 10,
		slidesPerView: 4,
		// loop: true,
		freeMode: true,
		// loopedSlides: 5, //looped slides should be the same
		watchSlidesVisibility: true,
		watchSlidesProgress: true,
	});
	var galleryTop1 = new Swiper('.gallery-top-1', {
		spaceBetween: 10,
		// loop:true,
		// loopedSlides: 5, //looped slides should be the same
		thumbs: {
			swiper: galleryThumbs1,
		},
	});
	var galleryThumbs2 = new Swiper('.gallery-thumbs-2', {
		spaceBetween: 10,
		slidesPerView: 4,
		// loop: true,
		freeMode: true,
		// loopedSlides: 5, //looped slides should be the same
		watchSlidesVisibility: true,
		watchSlidesProgress: true,
		observer: true,
		observeParents: true,
	});
	var galleryTop2 = new Swiper('.gallery-top-2', {
		spaceBetween: 10,
		// loop:true,
		// loopedSlides: 5, //looped slides should be the same
		thumbs: {
			swiper: galleryThumbs2,
		},
		observer: true,
		observeParents: true,
	});
	var galleryThumbs3 = new Swiper('.gallery-thumbs-3', {
		spaceBetween: 10,
		slidesPerView: 4,
		// loop: true,
		freeMode: true,
		// loopedSlides: 5, //looped slides should be the same
		watchSlidesVisibility: true,
		watchSlidesProgress: true,
		observer: true,
		observeParents: true,
	});
	var galleryTop3 = new Swiper('.gallery-top-3', {
		spaceBetween: 10,
		// loop:true,
		// loopedSlides: 5, //looped slides should be the same
		thumbs: {
			swiper: galleryThumbs3,
		},
		observer: true,
		observeParents: true,
	});

	var map;
	DG.then(function () {
		map = DG.map('map', {
			center: [54.98, 82.89],
			zoom: 13
		});
	});

	$('.top-line-center span').on('click', function()  {
		$('.select-form').slideToggle('400');
		$('.top-line-center span .fa').toggleClass('arrows-active');
	});

	$('#phone-id').mask('+7(999) 999-99-99',{placeholder: "+7 (   )   -  -  "});

	//E-mail Ajax Send
	$("form").submit(function() { //Change
		var th = $(this);
		$.ajax({
			type: "POST",
			url: "mail.php", //Change
			data: th.serialize()
		}).done(function() {
			$(location).attr('href', '/thanks.html');
		});
		return false;
	});



});
